import { BudgetModel } from "./models/budget.interface";

export const budget: BudgetModel = {
    "id": 1,
    "date": new Date(2018, 0, 15),
    "name": "My Budget",
    "balance": 4000,
    "totalBudgeted": 0,
    "availableToBudget": 0,
    "creditCardPayments": [

    ] ,
    "masterCategories": [
        {
            "id": 1,
            "name": "Immediate Obligations",
            "subCategories": [
                {
                    "id": 1, 
                    "name": "Mortgage",
                    "budgeted": 1000, 
                    "activity": -500,
                    "available": 500
                }
            ]
        },
        {
            "id": 2,
            "name": "True Expenses",
            "subCategories": [
                {
                    "id": 1, 
                    "name": "Home Maintenance",
                    "budgeted": 100, 
                    "activity": 0,
                    "available": 100
                }
            ] 
        },
        {
            "id": 3,
            "name": "Just For Fun",
            "subCategories": [
                {
                    "id": 1, 
                    "name": "Shopping",
                    "budgeted": 200, 
                    "activity": -30,
                    "available": 170
                },
                {
                    "id": 2, 
                    "name": "Dining Out",
                    "budgeted": 300, 
                    "activity": -50,
                    "available": 250
                }
            ] 
        }
    ]
    
}