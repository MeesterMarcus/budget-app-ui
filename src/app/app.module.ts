import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from "./app-routing.module";
import { BudgetComponent } from "./budget/budget.component";
import { AccountComponent } from "./account/account.component";
import { ReportsComponent } from "./reports/reports.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import 'chart.js';
import { ChartsModule } from 'ng2-charts';
import { AllAccountsComponent } from './all-accounts/all-accounts.component';
import { BudgetService } from "./services/budget.service";
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BudgetComponent,
    AccountComponent,
    ReportsComponent,
    AllAccountsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    ChartsModule,
    HttpClientModule
  ],
  providers: [BudgetService],
  bootstrap: [AppComponent]
})
export class AppModule { }
