import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { LoginComponent } from "./login/login.component";
import { BudgetComponent } from "./budget/budget.component";
import { AccountComponent } from "./account/account.component";
import { ReportsComponent } from "./reports/reports.component";
import { AllAccountsComponent } from "./all-accounts/all-accounts.component";

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  {
    path: 'budget',
    component: BudgetComponent
  },
  {
    path: 'account',
    component: AccountComponent
  },  
  {
    path: 'all_accounts',
    component: AllAccountsComponent
  },
  {
    path: 'reports',
    component: ReportsComponent
  },
  { 
    path: '',
    redirectTo: '/budget',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}