export interface SubCategoryModel {
    id: number;
    name: string;
    budgeted: number;
    activity: number;
    available: number;
}