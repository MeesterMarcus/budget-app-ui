import { SubCategoryModel } from "./sub-category.interface";

export interface MasterCategoryModel {
    id: number;
    name: string;
    subCategories: Array<SubCategoryModel>;
}