import { MasterCategoryModel } from "./master-category.interface";

export interface BudgetModel {
    id: number;
    balance: number;
    totalBudgeted: number;
    availableToBudget: number;
    date: Date;
    name: string;
    creditCardPayments: Array<any>;
    masterCategories: Array<MasterCategoryModel>;
}