export interface TransactionModel {
    date: string;
    payee: string;
    category: string;
    outflow: number;
    inflow: number;
    memo: string;
}