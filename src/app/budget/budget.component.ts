import { Component, OnInit } from '@angular/core';
import { MasterCategoryModel } from "../models/master-category.interface";
import { BudgetModel } from "../models/budget.interface";
import { budget } from "../budgets";
import { BudgetService } from "../services/budget.service";

@Component({
  selector: 'app-budget',
  templateUrl: './budget.component.html',
  styleUrls: ['./budget.component.css']
})
export class BudgetComponent implements OnInit {  

  currentBudget: BudgetModel;
  budgetDate: Date;

  constructor(private budgetService: BudgetService) { }

  ngOnInit() {
    this.currentBudget = budget;    
    this.calculateBudget();
    this.budgetService.testApi().subscribe(
      data => {
        console.log(data);
      }
    )
  }

  calculateBudget() {
    let totalBudgeted = 0;
    let availableToBudget = 0;
    
    for (let masterCategory of this.currentBudget.masterCategories) {
      for (let subCategory of masterCategory.subCategories) {
        totalBudgeted += subCategory.budgeted;
      }
    }

    availableToBudget = this.currentBudget.balance - totalBudgeted;
    this.currentBudget.totalBudgeted = totalBudgeted;
    this.currentBudget.availableToBudget = availableToBudget;
    console.log('totalBudgeted: ', totalBudgeted);
    console.log('availableToBudget: ', availableToBudget);
  }

  focusOutFunction() {
    this.calculateBudget();
  }

}
