import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { TransactionModel } from "../models/transaction.interface";

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  currentTransaction: TransactionModel;

  constructor() {}

  ngOnInit() {
    this.currentTransaction = {} as TransactionModel;

  }

  addTransaction() {
    console.log(this.currentTransaction);
  }

}
